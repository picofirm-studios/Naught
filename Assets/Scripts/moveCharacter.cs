using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCharacter : MonoBehaviour
{
    CharacterController characterController;
    public float MovementSpeed = 75.0f;
    public float Gravity = 8.0f;
    private float Velocity = 0.0f;
    private Camera cam;

    private void Start() {
        characterController = GetComponent<CharacterController>();
        cam = Camera.main;
    }
    void Update() {
        float horizontal = Input.GetAxis("Horizontal") * MovementSpeed * 2.5f;
        float vertical = Input.GetAxis("Vertical") * MovementSpeed * 3.5f;
        characterController.Move((cam.transform.right * horizontal + cam.transform.forward * vertical) * Time.deltaTime);
        if (characterController.isGrounded) {
            Velocity = 0;
        }
        else {
            Velocity -= Gravity * Time.deltaTime;
            characterController.Move(new Vector3(0, Velocity, 0));
        }
    }
}