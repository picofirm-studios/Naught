using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCamera : MonoBehaviour
{
    public float horizontalSpeed = 50f;
    public float verticalSpeed = 25f;
    private float xRotation = 150f;
    private float yRotation = 75f;
    private Camera cam;

    void Start() {
        #if UNITY_EDITOR
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
        if (Input.GetButton("Cancel")) {
            Cursor.lockState = CursorLockMode.None;
        }
        #else
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        #endif
        cam = Camera.main;
    }
    void Update() {
        float mouseX = Input.GetAxis("Mouse X") * horizontalSpeed * 2.5f;
        float mouseY = Input.GetAxis("Mouse Y") * verticalSpeed;
        yRotation += mouseX;
        xRotation += mouseY;
        xRotation = Mathf.Clamp(xRotation, -40, 40);
        cam.transform.eulerAngles = new Vector3(xRotation, yRotation, 0.0f);
    }
}