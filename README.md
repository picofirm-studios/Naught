## NAUGHT ALPHA v0.0.1
### NAUGHT - A Psychological Horror and First-Person Shooter by Picofirm Studios
> Naught is Picofirm Studios' first ever game, built lovingly by hand with care by our international team of independent developers at Picofirm Studios.
> So far, our game is merely a tech demo of our code we've written so far, and we'll update this repo as time goes on.

### NEWEST UPDATES:
- Camera now rotates a full 360 degrees and the Cursor locks to the center of the screen
- Camera spawns in the proper position and with realistic Y axis rotation
- Movement speed on Camera and Character increased to feel less sluggish
- Character no longer rotates on Y and Z axis, can only rotate on X axis
- Floor Physics has been touched up

### CURRENT BUGS:
- Jumping is still being programmed
- Collision is still not 100% there
